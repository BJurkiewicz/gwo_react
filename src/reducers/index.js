import {combineReducers} from 'redux'
import books from './books'

const appStore = combineReducers({
    books
})

export default appStore