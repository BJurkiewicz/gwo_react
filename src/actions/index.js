export const setBooks = books => {
    return {
        type: 'SET_BOOKS',
        books
    }
}