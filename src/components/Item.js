import React, {Component} from 'react'
import '../styles/css/Item.css'

class Item extends Component {
    render () {
        return (
        <li className="col-sm-12 col-lg-6 col-xl-4">
            <div className="card">
              <div className="row">
                <div className="card-image col-md-4 col-xl-12">
                  <img src={this.props.data.cover} className="card-img-top" alt={this.props.data.title} />
                </div>
                <div className="card-body col-md-8 col-xl-12">
                  <header>
                    <h4 className="card-title">{this.props.data.title}</h4>
                    <h5 className="card-text">{this.props.data.author}</h5>
                  </header>
                  <dl>
                    <dt className="card-text">Numer ISBN:</dt>
                    <dd>{this.props.data.isbn}</dd>
                    <dt className="card-text">numer dopuszczenia MEN:</dt>
                    <dd>{this.props.data.men}</dd>
                    <dt className="card-text">Liczba stron:</dt>
                    <dd>{this.props.data.pages_count}</dd>
                    <dt className="card-text">Przedmiot:</dt>
                    <dd>{this.props.data.subject}</dd>
                    <dt className="card-text">Rodzaj:</dt>
                    <dd>{this.props.data.type}</dd>
                    <dt className="card-text">Poziomy nauczania:</dt>
                    {this.props.data.levels.map((level, index) => (
                        <dd key={index}>
                            {level.school} - {level.class}
                        </dd>
                    ))}
                  </dl>
                  <footer>
                    <a href={this.props.data.url} className="btn btn-primary btn-lg btn-block btn-outline-primary" target="_blank">Przejdź do księgarni</a>
                  </footer>
                </div>
              </div>
            </div>
        </li>
        )
    }
}

export default Item