import React, { Component } from 'react';
import {connect} from 'react-redux'
import Form from './components/Form'
import Item from './components/Item'
import logo from './logo_gwo.png';
import './styles/css/App.css';

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      books: []
    }
  }

  render() {
    return (
      <div className="App">
        <img src={logo} className="logo" alt="Logo Gdańskiego Wydawnictwa Oświatowego" />
        <Form />
        <div className="container">
          <ul className="row">
            {
              this.props.books.map((book, index) => (
                <Item key={index} data={book} />
              ))
            }
          </ul>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    books: state.books
  }
}

export default connect(mapStateToProps, null)(App);
