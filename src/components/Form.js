import React, {Component} from 'react'
import {connect} from 'react-redux'
import axios from 'axios'
import {setBooks} from '../actions'
import Loader from './Loader'
import '../styles/css/Form.css'

class Form extends Component {
    constructor(props) {
        super(props)
        this.state = {
            text: '',
            loader: false
        }
    }
    handleChange = e => {
        this.setState({text: e.target.value})
    }
    submit = e => {
        e.preventDefault()
        this.setState({
            text: '',
            loader: true
        })
        return axios.get('https://gwo.pl/booksApi/v1/search?query=' + this.state.text)
        .then(res => {
            this.props.setBooks(res.data)
            this.setState({loader: false})
        })
        .catch(err => {
            console.error(err)
        })
    }
    render () {
        return (
            <form className="form container" onSubmit={this.submit}>
                <div className="form-group">
                    <input id="searchInput" className="form-control" value={this.state.text} type="text" onChange={this.handleChange} />
                    <label htmlFor="searchInput">Wpisz frazę</label>
                    <input type="submit" className="btn btn-outline-primary" value="Szukaj" />
                </div>
                {this.state.loader && 
                    <Loader />
                }
            </form>
        )
    }
    componentDidMount () {
        let input = document.getElementById('searchInput')
        input.focus()
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setBooks: query => {
            dispatch(setBooks(query))
        }
    }
}



export default connect(null, mapDispatchToProps)(Form)